﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Amazon.S3;
using Moq;
using Amazon.S3.Model;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;

namespace Tests
{
    [TestClass]
    public class S3FileDownloaderTests
    {

        [TestMethod]
        public void DownloadFiles()
        {
            var clientMock = new Mock<IAmazonS3>();
            var storageMock = new Mock<IStorage>();
            var filterMock = new Mock<IFilter<DateTime>>();
            var listObjectsResponse = new ListObjectsResponse { S3Objects = new List<S3Object> { new S3Object(), new S3Object() } };
            var objectResponse = new GetObjectResponse { ResponseStream = new MemoryStream() };

            filterMock.Setup(x => x.Apply(It.IsAny<DateTime>())).Returns(true);
            clientMock.Setup(x => x.ListObjects(It.IsAny<ListObjectsRequest>())).Returns(listObjectsResponse);
            clientMock.Setup(x => x.GetObject(It.IsAny<GetObjectRequest>())).Returns(objectResponse);

            S3FileDownloader sut = new S3FileDownloader(clientMock.Object, storageMock.Object, filterMock.Object);
            sut.DownloadFiles();

            clientMock.Verify(x => x.ListObjects(It.IsAny<ListObjectsRequest>()));
            clientMock.Verify(x => x.GetObject(It.IsAny<GetObjectRequest>()), Times.Exactly(2));
            storageMock.Verify(x => x.Save(It.IsAny<Stream>()), Times.Exactly(2));
        }
    }

    [TestClass]
    public class AppTests
    {
        [TestMethod]
        public void Download_SendMail_Clean()
        {
            var cleanMock = new Mock<ICleaner>();
            var mailMock = new Mock<IMailer>();
            var downloaderMock = new Mock<IFileDownloader>();
            var sut = new App(downloaderMock.Object,mailMock.Object, cleanMock.Object);

            sut.Download().SendMail().Clean();

            downloaderMock.Verify(x => x.DownloadFiles());
            mailMock.Verify(x => x.Send(It.IsAny<string>(),It.IsAny<string>()));
            cleanMock.Verify(x => x.Clean());

        }
    }

    public class S3FileDownloader : IFileDownloader
    {
        private IAmazonS3 _amazonClient;
        private IStorage _storage;
        private IFilter<DateTime> _filter;
        public S3FileDownloader(IAmazonS3 amazonClient, IStorage storage, IFilter<DateTime> filter)
        {
            _amazonClient = amazonClient;
            _storage = storage;
            _filter = filter;
        }
        public void DownloadFiles()
        {
            var listObjects = _amazonClient.ListObjects(new Amazon.S3.Model.ListObjectsRequest
            {
                BucketName = "virtualmind"
            });

            listObjects.S3Objects.AsQueryable()
                .Where(x => _filter.Apply(x.LastModified))
                .ToList()
                .ForEach(x => this.DownloadFile(x));
        }

        private void DownloadFile(S3Object s3Object)
        {
            var item = _amazonClient.GetObject(new GetObjectRequest { Key = s3Object.Key });
            _storage.Save(item.ResponseStream);
        }
    }

    public class App
    {
        private readonly IFileDownloader _fileDownloader;
        private readonly IMailer _mailer;
        private readonly ICleaner _cleaner;
        public App(IFileDownloader fileDownloader, IMailer mailer, ICleaner cleaner)
        {
            _fileDownloader = fileDownloader;
            _mailer = mailer;
            _cleaner = cleaner;
        }
        public App Download()
        {
            _fileDownloader.DownloadFiles();
            return this;
        }
        public App SendMail()
        {
            _mailer.Send(string.Empty,string.Empty);
            return this;
        }
        public void Clean()
        {
            _cleaner.Clean();
        }
    }

    public interface ICleaner
    {
        void Clean();
    }
    public interface IMailer
    {
        void Send(string body, string subject);
    }
    public interface IFileDownloader
    {
        void DownloadFiles();
    }
    public interface IFilter<T>
    {
        bool Apply(T value);
    }
    public interface IStorage
    {
        void Save(Stream stream);
    }
}
